package com.example.notificationtesting;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;

class MyBroadcastReceiver extends BroadcastReceiver {
    private static final String ACTION = "notifme";

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notifAnjeng = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        String channelId = "app_channel_id";
        String channelName = "app_channel_name";
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel notifChannel = new NotificationChannel(channelId, channelName, importance);
        notifAnjeng.createNotificationChannel(notifChannel);

        Intent intent1 = new Intent(context,MainActivity.getClass());
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,100,intent1,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,channelId)
                .setContentIntent(pendingIntent)
                .setContentTitle("ANJING FRANNN")
                .setContentText("I MISS U -azzuri.vsn");

        notifAnjeng.notify(101,builder.build());
    }
}
