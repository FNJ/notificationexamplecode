package com.example.notificationtesting;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createNotificationChannel();

    }

    /*
    This function will make a short duration notification showed on screen
     */
    public void toastNotify(View view){
        // You can CHANGE on Toast.LENGTH_SHORT to modify how long the notification will showed
        Toast.makeText(getBaseContext(), "Example Of Toast Notification", Toast.LENGTH_SHORT).show();
    }

    public void simpleNotificationUseManager(View view){
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(this, ResultActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "good")
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setContentTitle("New notif")
                .setContentText("good one")
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);


        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(1, builder.build());
    }
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.test);
            String description = getString(R.string.test);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("good", "my", importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void ManyThingNotification(View view){

        // Create Manager
        NotificationManager aNotificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent intent = new Intent(getApplicationContext(), ResultActivity.class);

        // Creating a pending intent and wrapping our intent
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 1,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Create Notification Object
        NotificationCompat.Builder noti = new NotificationCompat.Builder(getApplicationContext(), "test")
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setWhen(System.currentTimeMillis())  			//When the event occurred
                .setContentTitle("Title")   				//Title message top row.
                .setPriority(Notification.PRIORITY_MAX)
                .setContentText("good")  	//message
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);   						//allow auto cancel when pressed.


        // Send Notification Object to Manager
        aNotificationManager.notify(1, noti.build());      //finally build and return a Notification.
    }
}
